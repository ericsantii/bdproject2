/*
 * Parse the data and create a graph with the data.
 */
function parseData(createGraph) {
    Papa.parse("../data/topposters.csv", {
    download: true,
    complete: function(results) {
    createGraph(results.data);
    }
});
    }
    function createGraph(data) {
var words = [];
    var frequency = [];
    var wordlist = [];
    var date = [];
for (var i = 0; i < data.length-1; i++) {
    var word2add = [];
    word2add.push(data[i][0]);
    for(var j = 0; j<=i;j++){
        if(j<i){
            word2add.push(null);
        }
        else if(j==i){
            word2add.push(data[j][1]);
        }

    }
    wordlist.push(word2add);
    words.push(data[i][0]);
    frequency.push(data[i][1]);
    date.push(data[i][2]);
    
}

console.log(wordlist);
console.log(frequency);
console.log (date);

var chart = c3.generate({

    data: {
        columns:
            wordlist
        ,
        type : 'scatter',
        onclick: function (d, i) { console.log("onclick", d, i); },
        onmouseover: function (d, i) { console.log("onmouseover", d, i); },
        onmouseout: function (d, i) { console.log("onmouseout", d, i); }
    },
    axis: {
        x: {
            type:'category',
            categories:date,
            tick: {
multiline: false,
culling: {
    max: 10
}
}
},
        
    }
});
}

parseData(createGraph);
