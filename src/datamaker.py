import os
import datetime
import pandas as pd


def find_csv_filenames( path_to_dir, suffix=".csv" ):
    filenames = os.listdir(path_to_dir)
    for filename in filenames:
        if filename.endswith(suffix):
            return filename


file_list = os.listdir("mydata")


def tophashtags():
    for file in file_list:
        if "tophashtags" in file:
            date = int(file[-13:])
            realdate = datetime.datetime.fromtimestamp(date // 1000.0)
            csv_input = pd.read_csv("mydata/" + file + "/" + find_csv_filenames("mydata/" + file + "/"),
                                    names=['Hashtag', 'Frequency', 'Date'])
            csv_input['Date'] = realdate
            csv_input.to_csv('hashtags.csv', index=False, mode='a', header=False)
            
            
def topspecific():
    for file in file_list:
        if "topspecific" in file:
            date = int(file[-13:])
            realdate = datetime.datetime.fromtimestamp(date // 1000.0)
            csv_input = pd.read_csv("mydata/" + file + "/" + find_csv_filenames("mydata/" + file + "/"),
                                    names=['Hashtag', 'Frequency', 'Date'])
            csv_input['Date'] = realdate
            csv_input.to_csv('topspecific.csv', index=False, mode='a', header=False)
            
    
def topkeywords():
    for file in file_list:
        if "topkeywords" in file:
            date = int(file[-13:])
            realdate = datetime.datetime.fromtimestamp(date // 1000.0)
            csv_input = pd.read_csv("mydata/" + file + "/" + find_csv_filenames("mydata/" + file + "/"),
                                    names=['Hashtag', 'Frequency', 'Date'])
            csv_input['Date'] = realdate
            csv_input.to_csv('topkeywords.csv', index=False, mode='a', header=False)
            
    
def topposters():
    for file in file_list:
        if "topposter" in file:
            date = int(file[-13:])
            realdate = datetime.datetime.fromtimestamp(date // 1000.0)
            csv_input = pd.read_csv("mydata/" + file + "/" + find_csv_filenames("mydata/" + file + "/"),
                                    names=['Hashtag', 'Frequency', 'Date'])
            csv_input['Date'] = realdate
            csv_input.to_csv('topposters.csv', index=False, mode='a', header=False)