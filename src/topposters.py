#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pyspark.sql import SQLContext, SparkSession
from pyspark.sql.functions import *
from pyspark.sql.types import *

spark = SparkSession.builder.getOrCreate()


df = spark.read.json('/eric/python.json')
df.createOrReplaceTempView('twitter_tw')
lower = df.select(min(df['timestamp_ms']).alias('minimum')).first()['minimum']
hour = 1
while(hour<=57):
        upperBound = str(int(lower) + (12 * 3600000))
        countedIDs = spark.sql("select user.id_str as user, count(*) as count from twitter_tw where timestamp_ms >= {} and timestamp_ms < {} group by user order by count desc limit 10".format(lower, upperBound)).write.csv('/eric/topposter12Hrs-hour-' + str(hour) + '-' + str(upperBound))
        lower = str(int(lower) + 3600000)
        hour += 1
