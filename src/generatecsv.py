import datamaker as dt


def start():
    dt.tophashtags()
    dt.topkeywords()
    dt.topposters()
    dt.topspecific()


if __name__ == '__main__':
    start()